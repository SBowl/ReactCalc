# ReactCalc

### Description
A simple calculator made using React.

### Running
1. Install Node.js and NPM if you don't have them already (https://nodejs.org/en/download/)
2. From your terminal:

```bash
    cd ./<your_directories>/ReactCalc/
    npm install
    npm run build
    npm start
```

3. Open your browser at localhost:8080
4. Have fun!