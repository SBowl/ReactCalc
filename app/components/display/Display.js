'use strict';

import React from 'react';
import display from './display.css';

export default class Display extends React.Component {
  render() {
    return (
      <div className={display.display}>
        <p className={display.row}>R: {this.props.result}</p>
        <p className={display.row}>N: {this.props.num}</p>
      </div>
    );
  }
}

Display.propTypes = {
  num: React.PropTypes.string,
  result: React.PropTypes.string,
};
