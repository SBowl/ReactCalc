'use strict';

import React from 'react';
import Display from '../components/display/Display';
import btn from './buttons.css';
import layout from './layout.css';

export default class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      operator: '', // operator
      num: '', // number to operate with
      result: '', // result or first number
    };

    this.newOperation = true; // see if result should be '' at next num insertion
    this.firstPoint = true; // avoid point duplication
    this.toCalc = true; // avoid double calculations
    this.sum = this.sum.bind(this);
    this.subtract = this.subtract.bind(this);
    this.divide = this.divide.bind(this);
    this.multiply = this.multiply.bind(this);
    this.calculate = this.calculate.bind(this);
    this.handleNumber = this.handleNumber.bind(this);
    this.handleOperator = this.handleOperator.bind(this);
    this.handleEquals = this.handleEquals.bind(this);
  }

  clearAll() {
    this.newOperation = true;
    this.firstPoint = true;
    this.toCalc = true;

    this.setState({
      operator: '',
      num: '',
      result: '',
    });
  }

  setResultAsNum() {
    var number = this.state.num;

    this.setState({
      result: number,
    });
  }

  sum() { // sum the result with the new number
    var number = parseFloat(this.state.num);
    var resultV = parseFloat(this.state.result);
    var newResult = resultV + number;
    this.setState({
      result: newResult.toString(),
    });
  }

  subtract() { // subtract the result with the new number
    var num = parseFloat(this.state.num);
    var resultV = parseFloat(this.state.result);
    var newResult = resultV - num;
    this.setState({
      result: newResult.toString(),
    });
  }

  divide() { // divide the result with the new number
    var num = parseFloat(this.state.num);
    var resultV = parseFloat(this.state.result);
    var newResult = resultV / num;
    this.setState({
      result: newResult.toString(),
    });
  }

  multiply() { // multiply the result with the new number
    var num = parseFloat(this.state.num);
    var resultV = parseFloat(this.state.result);
    var newResult = resultV * num;
    this.setState({
      result: newResult.toString(),
    });
  }

  handleNumber(value) { // handle inserting a new number
    var numV = this.state.num;
    var newNum = value;
    this.toCalc = true;

    if (newNum === '.' && this.firstPoint) {
      this.firstPoint = false;
      if (numV === '') {
        this.setState({
          num: '0' + newNum,
        });
      } else {
        this.setState({
          num: numV + newNum,
        });
      }
    } else if (newNum != '.') {
      this.setState({
        num: numV + newNum,
      });
    }
  }

  handleOperator(value) { // handle inserting operator
    var newOperator = value;
    this.firstPoint = true;
    this.newOperation = false;

    if (this.toCalc) {
      this.calculate();
      this.toCalc = false;
    }

    this.setState({
      operator: newOperator,
      num: '',
    });
  }

  handleEquals() { // handle inserting equals
    this.firstPoint = true;
    this.newOperation = true;

    if (this.toCalc) {
      this.calculate();
      this.toCalc = false;
      this.setState({
        operator: '',
        num: '',
      });
    }
  }

  calculate() { // decides the calulation based on operator
    var operator = this.state.operator;
    switch (operator) {
      case '+':
        this.sum();
        break;
      case '-':
        this.subtract();
        break;
      case '*':
        this.multiply();
        break;
      case '/':
        this.divide();
        break;
      case '':
        this.setResultAsNum();
        break;
    }
  }

  render() {
    return (
      <div className={layout.outerBlock}>
        <div className={layout.block}>
          <Display result={this.state.result} num={this.state.num} />
          <div className={layout.row}>
            <div className={btn.white} onClick={() => this.handleNumber('7')}>
              7
            </div>
            <div className={btn.white} onClick={() => this.handleNumber('8')}>
              8
            </div>
            <div className={btn.white} onClick={() => this.handleNumber('9')}>
              9
            </div>
            <div className={btn.grey} onClick={() => this.handleOperator('/')}>
              ÷
            </div>
            <div className={btn.orange} onClick={() => this.clearAll()}>
              C
            </div>
          </div>
          <div className={layout.row}>
            <div className={btn.white} onClick={() => this.handleNumber('4')}>
              4
            </div>
            <div className={btn.white} onClick={() => this.handleNumber('5')}>
              5
            </div>
            <div className={btn.white} onClick={() => this.handleNumber('6')}>
              6
            </div>
            <div className={btn.grey} onClick={() => this.handleOperator('*')}>
              x
            </div>
          </div>
          <div className={layout.row}>
            <div className={btn.white} onClick={() => this.handleNumber('1')}>
              1
            </div>
            <div className={btn.white} onClick={() => this.handleNumber('2')}>
              2
            </div>
            <div className={btn.white} onClick={() => this.handleNumber('3')}>
              3
            </div>
            <div className={btn.grey} onClick={() => this.handleOperator('-')}>
              -
            </div>
          </div>
          <div className={layout.row}>
            <div className={btn.white} onClick={() => this.handleNumber('0')}>
              0
            </div>
            <div className={btn.white} onClick={() => this.handleNumber('.')}>
              .
            </div>
            <div className={btn.blue} onClick={this.handleEquals}>
              =
            </div>
            <div className={btn.grey} onClick={() => this.handleOperator('+')}>
              +
            </div>
          </div>
        </div>
      </div>
    );
  }
}
